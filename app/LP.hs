{-# LANGUAGE ScopedTypeVariables #-}

module LP where
import Data.Foldable (maximumBy)
import Data.Ord (comparing)

-- | A single Linear Program that can be solved
-- Find the vector x
-- that maximizes c . x
-- subject to ax <= b
-- and x >= 0
data LP num = LP
  { a :: ![[num]],
    b :: ![num],
    c :: ![num]
  }

class (Num num, Ord num, Fractional num) => LPNum num

-- | Solve an LP using the Simplex method
solve :: forall num. LPNum num => LP num -> [num]
solve lp@LP {a = a', b = b', c = c'} =
  let
    -- augment the LP with a
    tableau :: LP num
    tableau = lp { a = augment a' (identity a')
                 , c = c' ++ zeroes a'
                 }

    initial :: [num]
    initial = zeroes c' ++ b'

    improve :: (LP num, [num])-> (LP num, [num])
    improve (lp, xs) =
      let
        -- the most influencial variable
        m = maximumIndex (c lp)
        
        -- how far we can change this variable until we hit another bound
        u = maximum . zipWith (/) (b lp) $ map (!! m) (a lp) 

        -- 

      in ( lp { a = a lp
              }
         , xs )

  in
    undefined


-- Matrix Functions

-- | A zerovector as long as the given list
zeroes :: Num num => [a] -> [num]
zeroes = map (const 0)

-- | An identity matrix as big as the given list
identity :: Num num => [a] -> [[num]]
identity [] = []
identity (x:xs) = (1 : repeat 0) : fmap (0 :) (identity xs)

-- | Augment a matrix (A) with another (B), resulting in (A|B)
augment :: [[num]] -> [[num]] -> [[num]]
augment = zipWith (++)

maximumIndex :: Ord a => [a] -> Int
maximumIndex = fst . maximumBy (comparing snd) . zip [0..]